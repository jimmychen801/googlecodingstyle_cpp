# 函數名稱 - 小寫動作詞開頭

> 動作在前: 存取子 (Accessor)
>
> 物件在後: 修改子 (Mutator)

函式以**小寫字母開頭**，

並在每一個新單字的首字使用大寫字母，

可使用底線分隔。

```c++
addTableEntry()		//Accessor: add
deleteUrl()			//Accessor: delete
openFileOrDie()		//Accessor: open
get_AR_MetaData()	//Accessor: get
```


