# 檔案名稱

> 檔案名稱應該完全使用小寫，並可以包含底線 (`_`) 或是橫槓 (`-`)。 關於這點請遵循各專案的慣例。 如果沒有可以遵循且一致的本地模式，偏好使用 `_`。

**一些可接受的檔案名稱範例：**

- `my_useful_class.cc`

- `my-useful-class.cc`

- `myusefulclass.cc`

- `myusefulclass_test.cc // _unittest 與 _regtest 兩種寫法都過時了`

  

**Debug Log file：**

這類檔案名設計時應加上時間戳記，例如:`my_log_<hhmmss>.cc`