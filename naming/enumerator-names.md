# 列舉器名稱

> 列舉器的值 (無論是否有限定範圍) 都應該要以[常數](constant-names.md)或者[巨集](macro-names.md)的方式命名：像是 `kEnumName` 或 `ENUM_NAME`。

可以的話，列舉器的值傾向於用[常數](constant-names.md)的方式命名，不過使用[巨集](macro-names.md)的方式也是可以接受的。

列舉器本身的名稱，例如 `UrlTableErrors` (與 `AlternateUrlTableErrors`)，是一種型別，因此使用**開頭大寫，大小寫混和**的命名方式。

```c++
enum UrlTableErrors {
  kOK = 0,						//Constant命名法
  kErrorOutOfMemory,
  kErrorMalformedInput,
};
enum AlternateUrlTableErrors {
  OK = 0,
  OUT_OF_MEMORY = 1,			//Macro命名法
  MALFORMED_INPUT = 2,
};
```

