# 常數名稱 - 開頭以小寫k

> 使用 `constexpr` 或 `const` 宣告的變數，以及在程式全期數值皆是固定的變數，其**名稱應該以「k」為開頭**，並使用大小寫混和的寫法。 底線可以用在少數無法使用大小寫明確分開的場合。例如：
> ```c++
> const int kDaysInAWeek = 7;
> const int kAndroid8_0_0 = 24;  // Android 8.0.0
> ```

