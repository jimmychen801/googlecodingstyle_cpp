## 通用命名規則 (General Naming Rules)

> 命名應該要具有敘述的能力；避免使用不常見縮寫。

讓一個名稱具有越清楚的描述越好，包括原因在內。 不要去想節省橫向的空間，因為讓你的程式碼能夠被新讀者馬上讀懂這事重要得多。 不要使用會讓專案以外的讀者看起來曖昧不明的縮寫，以及不要利用刪除一個單詞之間的字母來製造縮寫。 

**符合規定：**

```cpp
int price_count_reader;    // 不含縮寫
int num_errors;            // 「num」是很廣泛運用的慣例
int num_dns_connections;   // 大部份的人都知道「DNS」代表甚麼
int lstm_size;             // 「LSTM」是機器學習 (Machine Learning) 領域中常見的縮寫
```

**不太好：**

```cpp
int n;                     // ?
int nerr;                  // num_errors?
int n_comp_conns;          // num_computer_connections?
int pc_reader;             // 很多東西都可以縮寫成「pc」
int cstmr_id;              // 刪掉了一些中間字母
FooBarRequestInfo fbri;    // 這根本不是一個字
```

**暫時性的區域變數** 是可以接受的簡易命名，因為通常是用於過渡運算處理，變數有效範圍很短，這類多應用在:

1. for/while loop 的 iteration 步進:  像是 `i,j,k`。 

2. 迴圈中累計值，或是字串或array的index，像是 `tmpSum, tmpRun`。

   

**[駝峰式命名法](https://zh.wikipedia.org/wiki/%E9%A7%9D%E5%B3%B0%E5%BC%8F%E5%A4%A7%E5%B0%8F%E5%AF%AB)(Camel case or Pascal case):**

1. 以(大或小)寫字母作為開頭(大小可寫視不同型態類型定義規則)，之後每一個單詞的首字都使用大寫字母，當連續單字之間易混淆意思時，可以底線分隔。

   例如: `startRpcProcess()`，而不是 `STARTRPCProcess()`。
   
   例如: `Version8_0_0`，而不是 `Version800`。
   
   

**盡量不要使用阿拉伯數字**:

例如: 不要 `start10TimeAdd()`，盡量是`startTenTimeAdd()`。

例如: 不要 `num1; num2`，盡量是`firstNum, secondNum`。