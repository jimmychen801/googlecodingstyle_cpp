# 巨集(Macro)名稱

> **全部使用大寫與底線**命名。

```c++
#define ROUND(x) ...
#define PI_ROUNDED
```

> **Macro定義的內容，應該要包起來(braced):**
>
> 常數值: 
>
> ```
> #define PI_ROUNDED (3.14)
> #define MY_OFFSET  (14+5)
> ```
>
> 程式碼片段: (又稱 inline funciton)
>
> ```
> #define MY_FUNC(x) do{ ... }while(0)
> ```
>
> **使用do{ } while(0) 來包 程式碼 的好處:**
>
> 1. 提供程式碼包覆: 用{  }，可避免呼叫處因沒注意是macro而產生compile error
> 2. 引用處可以分號 '';'' 結尾
>
> ```
> #define AOO(arg) \			//not use bracing
>     do_stuff_here(arg); \
>     do_stuff_there(arg);
> 
> #define BOO(arg) \			//use bracing only
> 	{	\
>     do_stuff_here(arg); \
>     do_stuff_there(arg); \
>     }
>     
> #define COO(arg) \			//use do while()
> 	do { \
>     	do_stuff_here(arg); \
>    	    do_stuff_there(arg); \
> 	} while(0)
>   
> 呼叫處程式碼:
> if( A )  
> 	AOO(1)				//logical error
> else if()
> 	BOO(1);				//compile error, extra ';'
> else
> 	COO(1);				//no error
> 	
> 因為，上述展開結果:
> if( A )  
>     do_stuff_here(arg);
>     do_stuff_there(arg);	//out of "if" scope!
> else if()
> 	{
>     	do_stuff_here(arg);
>         do_stuff_there(arg);
> 	};						//compile erro, extra ;
> else
> 	do {
>     	do_stuff_here(arg);
>         do_stuff_there(arg);
> 	}while(0);
> ```
>
> 