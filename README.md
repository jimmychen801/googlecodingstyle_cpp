# Google C Style Guide Reference

建議瀏覽本文件工具:

1. 安裝Typora: https://typora.io/，並取得本檔案包
2. (暫時gitlab): https://gitlab.com/jimmychen801/googlecodingstyle_cpp



參考來源:

- Google C++ Style Guide [https://google.github.io/styleguide/cppguide.html][1]

------------------------------

- [標頭檔 (Header Files)](header-files/README.md)
    - [重載保護(#Define Guard)](header-files/define-guard.md)
- [🚧 變數宣告 ()](variable/variable_declare.md)
- [命名 (Naming)](naming/README.md)
    - [通用命名規則](naming/general-naming-rules.md)
    - [檔案(File)名稱](naming/file-names.md)
    - [型別(Type)名稱](naming/type-names.md)
    - [變數(Variable)名稱](naming/variable-names.md)
    - [常數(Constant)名稱](naming/constant-names.md)
    - [函數(Function)名稱](naming/function-names.md)
    - [列舉(Enum)名稱](naming/enumerator-names.md)
    - [巨集(Macro)名稱](naming/macro-names.md)
- [🚧 排版](code_indent.md)

------------------------------



