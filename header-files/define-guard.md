## The `#define` Guard

> 所有的標頭檔應該要包含 `#define` 保護，以**防止多重載入**。 
>
> 其名稱的格式為 `<專案名稱>_<路徑>_<檔名>_H_`。

**Example：**

```
liblhdc/include/lhdc_api.h
```



```c++
#ifndef __LHDC_API_H__
#define __LHDC_API_H__

...

#endif  // __LHDC_API_H__
```

**Failure Case:**

grand_parent.h

```C
int getFamilyNumber()
{
    return (5);
}
```

parent.h

```
#include "grand_parent.h"
```

main.c

```
#include "grand_parent.h"
#include "parent.h"

int main();
```

**After Resolving by Preprocessor:**

```c
int getFamilyNumber()
{
    return (5);
}
int getFamilyNumber()	//Duplicate definition by including parent.h
{
    return (5);
}
int main();
```

