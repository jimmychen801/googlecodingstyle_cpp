## Code Indentation

> **如果要修改的地方，已有別人的排版sytle，請參照之。**
>
> **完全是自己的檔案，可以套用我們自訂的預設。**
>
> 
>
> **預設規定項目:**
>
> 1. 縮排:  4個空格
> 2. Function 本身大括號
> 3. Function 內部statement

```c
void func()			//function本身
{
    int a = 0;
	if (sss) {		//statement最好都用有{ }包起來，即使只有一行執行碼，可避免引用到寫法不好的macro
        ...
    } else {
        ...
    }
    
    switch() {
        case 1:
		{
            ...
            break;
        }
        case 2:
		{
            ...
            break;
        }
        case 3:
		{
            ...
            break;
        }            
        default:
            break;
    }
}
```

**#if, #ifdef, #else, #endif**

```c
int func()
{
	int a = 0;
	int b = 0;

	for(...) {
#ifdef ADD_A
		a++;
#ifdef SUB_B
        b--;
#endif	//endif_SUB_B
#else	//else_SUB_A			//方便好找到對應的#ifdef
		b++;
#endif	//endif_ADD_A			//方便好找到對應的#ifdef
	}
}
```

