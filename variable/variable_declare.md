## Variable Declaration

> 1. 宣告處盡量立即初始化
> 2. 如果初始化值不能立即取得，應給個安全值
> 3. 盡量使用(brace initialization)
> 4. 不要在迴圈內做宣告變數

**宣告處盡量立即初始化：**

```c
int i;
i = f();      // Bad -- initialization separate from declaration.

int j = g();  // Good -- declaration has initialization.
```

**如果初始化值不便在當時取得，也應給予適合的初始值：**

```c
int i = 0;				//default value
char *ptr = NULL;		//default address

...
i = getValue();
ptr = (char *)string;
```

**盡量使用(brace initialization)：**

```c
std::vector<int> v;
v.push_back(1);  	// Prefer initializing using brace initialization.
v.push_back(2);

std::vector<int> v = {1, 2};  // Good -- v starts initialized.
```

**不要在迴圈內做宣告變數:**

```C
for(int i; i<k; i++)
{
    int j = k+1;	// Bad, declaration of vaiable j is invoked every time in loop
}


int m = 0;
for(int i; i<k; i++)
{
    m = k+1;		// Good, declaration of vaiable m is invoked one time only
}
```

